#!/bin/bash
#@dani_julian

usage="$(basename "$0") [-h] [-i input primers] [-m marker name] [-g species genomes] [-o output]

    -h  Show this help text
    -i  Fasta file with the primers to be analyzed
    -m  Marker name to be analyzed (examples COI, cytB, 16S...)
    -g  Fasta file with the species genomes to amplify with the input primers (ncbi index is needed)
    -t  OPTIONAL: number of threads to use. Default: 5 threads
    -o  Output directory name to store results"

threads=5

while getopts "i:m:g:o:p:t:h" opt; do
  case ${opt} in
    h ) echo "$usage"; exit;;
    i ) input=$OPTARG;;
    m ) marker=$OPTARG;;
    g ) genome=$OPTARG;;
    p ) primers=$OPTARG;;
    t ) threads=$OPTARG;;
    o ) DIR=$OPTARG;;
    : ) echo "Invalid option: $OPTARG requires an argument" >&2; echo "$usage" >&2; exit 1;;
    \? ) echo "Invalid option: $OPTARG. It is required an argument" >&2; echo "$usage" >&2; exit 1;;
  esac
done

#if no option is passed
if [ ! "$input" ] || [ ! "$genome" ] || [ ! "$DIR" ]  || [ ! "$marker" ]; then
  echo "arguments -i, -m, -b and -o must be provided"
  echo "$usage" >&2; exit 1
fi

#Get the path to this pipeline
path=$(echo $0 | awk 'BEGIN{FS=OFS="/"} {$NF=""; print "./"$0}')

#db=$(echo ${marker^^} | awk '{if ($0 == "12S" || $0 == "16S" || $0 == "COI" || $0 == "CYTB"){print "/media/dani_sequentia/HDD/Sequentia_proyectos/Metabarcoding_Illumina/Database/Mitochondria/Mito_db_ncbi_07_03_2022.fasta"} else if ($0 == "MATK" || $0 == "RBCL" || $0 == "TRNH-PSBA" || $0 == "TRNL"){print "/media/dani_sequentia/HDD/Sequentia_proyectos/Metabarcoding_Illumina/Database/Chloroplast/Chloro_db_ncbi_07_03_2022.fasta"} else {print "ERRR. Please use a correct marker in -m option"; exit 1}}')
#primer_pair=$(cat $input | tr "\t" "/")

#0. Create output directory
mkdir $DIR

#time
start=`date +%s`
echo "Start of analysis: " $(date) > $DIR"/log.file"

#1. Change degenarated primers sequences for all the possibilities
perl $path"/scripts/Cambio_degenerated_sequences.pl" $input && mv $PWD"/non_deg_seqs_primers.fasta" $DIR"/non_deg_seqs_primers.fasta"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during de-degeneration of primers" >&2 ; exit 1 ; fi

#2. Blast primers sequences against genomes
blastn -num_threads $threads -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs" -db $genome -query $DIR"/non_deg_seqs_primers.fasta" -task blastn -max_target_seqs 999999999 -word_size 7 > $DIR"/1.Results_raw_blast.out"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during primers alignment against genomes" >&2 ; exit 1 ; fi

#3. Join by pairs all the primers aligned in the same genome
python3 $path"/scripts/Join_pairs_of_primers.py" $DIR"/1.Results_raw_blast.out" > $DIR"/2.Results_join_primers_blast.out"
#3.1 Filter those pairs that do not integrate an amplicon (F-F, R-R or F-R without looking at each other), calculate amplicon distance and filter columns
cat $DIR"/2.Results_join_primers_blast.out" | awk 'BEGIN {FS=OFS="\t"} function abs(v) {return v < 0 ? -v : v} {if (($9 < $10 && $22 > $23 && $10 < $23) || ($9 > $10 && $22 < $23 && $10 > $23)){print $1,$14,$2,$9,$22,abs($22-$9)}}' > $DIR"/3.Results_regions_blast.out"
#3.2 Filter pairs of primers that aligned in sequences of the same species or consisted of an amplicon greater than 2000bp
cat $DIR"/tmp" | awk -F "\t" '$6 < 2000' > $DIR"/tmp"
#3.2 Filter pairs of primers that aligned in sequences of the same species or consisted of an amplicon greater than 2000bp
#cat $DIR"/3.Results_regions_blast.out" | awk -F "\t" '!seen[$1$2$3]++' > $DIR"/3.Results_regions_blast.out" && rm $DIR"/tmp"
#If the pair of primers create more than one amplicon...
mv $DIR"/tmp" > $DIR"/3.Results_regions_blast.out"

EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during pairing primers alingments and filtration" >&2 ; exit 1 ; fi

#4. Creation of bed file
cat $DIR"/3.Results_regions_blast.out" | awk 'BEGIN {FS=OFS="\t"} {print $3,$4,$5,$1"_AND_"$2}' > $DIR"/amplicons.bed"
#4.1 Fix bed file to get the lowest position as the start of the amplicon
cat $DIR"/amplicons.bed" | awk 'BEGIN {FS=OFS="\t"} {if ($2>$3){split ($4,a,"_AND_"); {print $1,$3,$2,a[2]"_AND_"a[1]}} else {print $0}}' > $DIR"/tmp" && mv $DIR"/tmp" $DIR"/amplicons.bed"
#4.2 Filter amplicon sequences constructed from the same pairs of primers in the same species
#python3 $path"/scripts/Filter_pairs_of_primers_on_same_species.py" $DIR"/amplicons.bed" > $DIR"/tmp" && mv $DIR"/tmp" $DIR"/amplicons.bed" #njo es necesario aqui ya que no hay mas de un genoma de la misma especie
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during bed file creation" >&2 ; exit 1 ; fi

#5. Get the best pair of primers
if [[ -n $primers ]]; then best_primers=$(cat $primers | sed 's/\t/_AND_/'); else best_primers=$(cat $DIR"/amplicons.bed" | cut -f 4 | sort -nr | head -n 1); fi #antes habia una opcion -p para elegir la pareja de primers, ahora siplemente se la doy en el input
best_primers_rev=$(echo $best_primers | awk -F "_AND_" '{print $2"_AND_"$1}')
grep -E "$best_primers|$best_primers_rev" $DIR"/amplicons.bed" > $DIR"/best_amplicons.bed"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during evaluation of best pair of primers" >&2 ; exit 1 ; fi

#6. Amplicon construction
bedtools getfasta -fi $genome -bed $DIR"/best_amplicons.bed" -name -fo $DIR"/Amplicon_sequences.fasta"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during amplicons creation" >&2 ; exit 1 ; fi

#7. Alignment amplicons against amplicons
#7.1 Amplicon Indexes
mkdir $DIR"/"$best_primers
dir=$DIR"/"$best_primers
mv $DIR"/Amplicon_sequences.fasta" $dir
makeblastdb -in $dir"/Amplicon_sequences.fasta" -input_type fasta -dbtype nucl
#7.2 Amplicon Alignments
blastn -num_threads $threads -outfmt "6 qseqid sseqid pident length mismatch gapopen gaps qstart qend sstart send evalue bitscore qcovs qlen" -db $dir"/Amplicon_sequences.fasta" -query $dir"/Amplicon_sequences.fasta" -task blastn -max_target_seqs 999999999 > $dir"/1.Amplicon_results_raw_blast.out"
#7.2.1 Filter hits with no more than 10% of amplicon coverage (short alignments)
cat $dir"/1.Amplicon_results_raw_blast.out" | awk '{if ($4/$15 >= 0.1) {print $0}}' > $dir"/tmp" && mv $dir"/tmp" $dir"/1.Amplicon_results_raw_blast.out"
#7.3 Filter alignment results
cat $dir"/1.Amplicon_results_raw_blast.out" | awk 'BEGIN {FS=OFS="\t"} {if ($1 != $2){print $0}}' > $dir"/tmp" #remove A vs A
python3 $path"/scripts/Filter_repetitions_from_blast.py" $dir"/tmp" > $dir"/2.Amplicon_results_filtered_blast.out" && rm $dir"/tmp" # remove AvsA/BvsA
#7.4 Calculate identity as (alignment_length-missmatches-gaps)/query_length
cat $dir"/2.Amplicon_results_filtered_blast.out" | awk 'BEGIN {FS=OFS="\t"} {print $1,$2,($4-$5-$7)*100/$NF}' > $dir"/3.Input_for_plot.tsv"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during amplicon alginments" >&2 ; exit 1 ; fi

#8. Get species name of each alignment hit
cat $dir"/2.Amplicon_results_filtered_blast.out" | cut -f 1 | awk -F "::" '{print $2}' |cut -d "_" -f 1 | taxonkit reformat -I 1 -f "{k};{p};{c};{o};{f};{g};{s};{t}" | cut -f 2 | awk -F ";" '{if ($NF==""){print $(NF-1)} else {print $NF}}' > $dir"/species_1.txt"
cat $dir"/2.Amplicon_results_filtered_blast.out" | cut -f 2 | awk -F "::" '{print $2}' |cut -d "_" -f 1 | taxonkit reformat -I 1 -f "{k};{p};{c};{o};{f};{g};{s};{t}" | cut -f 2 | awk -F ";" '{if ($NF==""){print $(NF-1)} else {print $NF}}' > $dir"/species_2.txt"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during getting species names" >&2 ; exit 1 ; fi

#9. Create final table
n=$(cat $dir"/2.Amplicon_results_filtered_blast.out" | wc -l) && seq 1 $n | while read line; do echo -e $marker"\t"$best_primers | sed 's/_AND_/\//'; done > $dir"/tmp"
echo -e "Marker\tPrimer pair\tIdentity\tSpecies 1\tSpecies 2" > $dir"/Results_table.tsv"
cat $dir"/3.Input_for_plot.tsv" | cut -f 3 | paste $dir"/tmp" - $dir"/species_1.txt" $dir"/species_2.txt" >> $dir"/Results_table.tsv"
#ssconvert $dir"/Results_table.tsv" $dir"/Results_table.xlsx"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during final table creation" >&2 ; exit 1 ; fi

#10. Boxplot creator for each species
Rscript $path"/scripts/Boxplot_creator.R" $dir "Results_table.tsv" $best_primers "Results_boxplot.png"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error during boxplot creation" >&2 ; exit 1 ; fi

#log file
end=`date +%s`
echo "End of analysis: " $(date) >> $DIR"/log.file"
total_time=$(bc <<< "scale=5;($end-$start)/60" | awk '{printf "%.2f",$1}')
echo -e "Complete Deliverable_3 analysis\t"$total_time"mins" >> $DIR"/log.file"