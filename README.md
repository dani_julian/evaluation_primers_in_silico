# Evaluation of discriminatory power of a pair of primers in metagenomics

## 0.- Download repository
```shell
git clone git@gitlab.com:dani_julian/evaluation_primers_in_silico.git
```
## 1.- Introduction
This pipeline creates amplicon regions from different species genomes using a pair of primers provided as input. After that, it is studied the similiraty (in terms of identity) among that amplicons in order to evaluate the capacity of discrimination. Results are presented in a final table and a boxplot. 
 
## 2.- Usage

| Argument | Explanation |
| :---: |     :---: |
| -h | Show this help text |
| -i | Fasta file with the primers to be analyzed | 
| -g | Fasta file with the species genomes to amplify with the input primers (ncbi index is needed) |
| -t | OPTIONAL: number of threads to use. Default: 5 threads |
| -o | Output directory name to store results | 

## 3.- Example
```
bash Evaluation_of_primers.sh -i cytB/Primers_degenerated_3.fasta -m cytB -g /media/dani_sequentia/HDD/Sequentia_proyectos/Meriuex/1.Atun/Atun_db/Thunnus_genome.fasta -t 10 -o cytB_results_thunnus_3

```
