#!/usr/bin/perl
#Author: Anne Beeston
#Creation Date: August 21, 2009
#Program Name: dcsp.pl (also called degtrans8.pl)
#Program Function: this program accepts a FastA file containing degenerate sequences,
#	generates all sequence combinations for each input sequence, and produces an
#	output file with all resulting sequences with unique names in FastA format.

use warnings;
use strict;

#declaration of all arrays
my @arrayofNames=();
my @AofEntries=();
my @newArray=();
my @cleanArray=();

#prompt user for name of input file and open file
#print "Enter the name of the input sequence file and then press enter: ";
my $filetoread = $ARGV[0];
chomp ($filetoread);
open (DNAFILE, $filetoread) or die ("Cannot open the file: $!");

#change default value of $/ from "\n" to ">" to read all lines until ">"
$/=">";

#loop through each sequence entry in the input file 
while (my $sequenceEntry =<DNAFILE>){
	#ignore first ">"
	if ($sequenceEntry eq ">"){
		next;}
	#remove sequence titles from sequence entry and place in array called arrayofNames
	my $sequenceTitle = "";
	if ($sequenceEntry =~ m/^([^\n]+)/){
		$sequenceTitle =$1;}
	else {$sequenceTitle = "No title was found!";}
		$sequenceEntry =~ s/[^\n]+//;
	push (@arrayofNames, $sequenceTitle);
	#remove all non DNA characters (ACGT and degenerate bases) from sequence entry
	$sequenceEntry =~ s/[^ACGTRYWSKMDVHBNacgtrywskmdvhbn]//g;
	#make all sequences uppercase
	$sequenceEntry =~tr/acgtrywskmdvhbn/ACGTRYWSKMDVHBN/;
	#add sequences to array called AofEntries
	push (@AofEntries, $sequenceEntry);
}

close (DNAFILE) or die ("Cannot close the file: $!");
open (FH, ">non_deg_seqs_primers.fasta") or die ("can't open the outfile: $!");

#this loop (and subloops) generate all combinations of each degenerate input sequence and place in newArray
for (my $i=0; $i<scalar(@arrayofNames);$i=$i+1){
	@newArray=();
	push (@newArray, $AofEntries[$i]);
	#loop through array to convert each sequence that contains "R" to 2 new sequences containing "A" or "G" 
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/R/){
		    my $temp = $newArray[$i];
		    $temp =~ s/R/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/R/G/;
		    push (@newArray, $temp2);
		}
	}
	#loop through array to convert each sequence that contains "Y" to 2 new sequences containing "C" or "T" 
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/Y/){
		    my $temp = $newArray[$i];
		    $temp =~ s/Y/C/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/Y/T/;
		    push (@newArray, $temp2);
		}
	}
	#loop through array to convert each sequence that contains "S" to 2 new sequences containing "C" or "G"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/S/){
		    my $temp = $newArray[$i];
		    $temp =~ s/S/C/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/S/G/;
		    push (@newArray, $temp2);
		}
	}
	#loop through array to convert each sequence that contains "W" to 2 new sequences containing "A" or "T"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/W/){
		    my $temp = $newArray[$i];
		    $temp =~ s/W/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/W/T/;
		    push (@newArray, $temp2);
		}
	}
	#loop through array to convert each sequence that contains "K" to 2 new sequences containing "G" or "T"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/K/){
		    my $temp = $newArray[$i];
		    $temp =~ s/K/G/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/K/T/;
		    push (@newArray, $temp2);
		}
	}
	#loop through array to convert each sequence that contains "M" to 2 new sequences containing "C" or "A"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/M/){
		    my $temp = $newArray[$i];
		    $temp =~ s/M/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/M/C/;
		    push (@newArray, $temp2);
		}
	}
	#loop through array to convert each sequence that contains "D" to 2 new sequences containing "A" or "G" or "T"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/D/){
		    my $temp = $newArray[$i];
		    $temp =~ s/D/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/D/G/;
		    push (@newArray, $temp2);
		    my $temp3 = $newArray[$i];
		    $temp3 =~ s/D/T/;
		    push (@newArray, $temp3);
		}
	}
	#loop through array to convert each sequence that contains "H" to 2 new sequences containing "A" or "C" or "T"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/H/){
		    my $temp = $newArray[$i];
		    $temp =~ s/H/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/H/C/;
		    push (@newArray, $temp2);
		    my $temp3 = $newArray[$i];
		    $temp3 =~ s/H/T/;
		    push (@newArray, $temp3);
		}
	}
	#loop through array to convert each sequence that contains "V" to 2 new sequences containing "A" or "C" or "G"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/V/){
		    my $temp = $newArray[$i];
		    $temp =~ s/V/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/V/C/;
		    push (@newArray, $temp2);
		    my $temp3 = $newArray[$i];
		    $temp3 =~ s/V/G/;
		    push (@newArray, $temp3);
		}
	}
	#loop through array to convert each sequence that contains "B" to 2 new sequences containing "C" or "G" or "T"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/B/){
		    my $temp = $newArray[$i];
		    $temp =~ s/B/C/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/B/G/;
		    push (@newArray, $temp2);
		    my $temp3 = $newArray[$i];
		    $temp3 =~ s/B/T/;
		    push (@newArray, $temp3);
		}
	}
	#loop through array to convert each sequence that contains "N" to 2 new sequences containing "A" or "C" or "G" or "T"
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		if ($newArray[$i] =~ m/N/){
		    my $temp = $newArray[$i];
		    $temp =~ s/N/A/;
		    push (@newArray, $temp);
		    my $temp2=$newArray[$i];
		    $temp2=~s/N/C/;
		    push (@newArray, $temp2);
		    my $temp3 = $newArray[$i];
		    $temp3 =~ s/N/G/;
		    push (@newArray, $temp3);
		    my $temp4=$newArray[$i];
		    $temp4=~s/N/T/;
		    push (@newArray, $temp4);
		}
	}
	#this loop removes all temporary elements in newArray that contain deg characters
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		#print"\n";
		#print $newArray[$i];
		if ($newArray[$i] =~m/[RYSWKMBDHVN]/){
			shift (@newArray);
			$i=-1;
		}
	}@cleanArray=();
	
	#this loop places all elements of newArray into cleanArray
	for (my $i=0;$i<scalar(@newArray);$i=$i+1){
		push (@cleanArray, $newArray[$i]);
	}
	#this loop prints all expanded probe sequences with unique probe names, to an outfile in FastA format 
	for (my $j=0; $j<scalar(@cleanArray);$j=$j+1){
		print FH ">".$arrayofNames[$i]."_".($j+1)."\n";
		print FH $cleanArray[$j]."\n";
	}
}
close FH;
#print "Open \"non_deg_probe_list.txt\" to view the results.\n";
