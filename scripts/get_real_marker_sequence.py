#! /bin/python3
#Get the best hits aligned against the same species
#@dani_julian

import sys

data = open(sys.argv[1], "rt")
dicc = {}

for line in data:
    line = line.rstrip().split("\t")
    hit = line[0].split(":")[0].split("_")[-1] + "_" + line[0].split(":")[2].split("_")[0]
    specie = hit.split("_")[1]
    bit_score = line[-1]
    if specie not in dicc:
        dicc[specie] = bit_score + "_" + hit
    elif float(bit_score) > float(dicc[specie].split("_")[0]):
        dicc[specie] = bit_score + "_" + hit

for line in dicc:
    print("_" + "::".join(dicc[line].split("_")[1:3]) + "_")
data.close()