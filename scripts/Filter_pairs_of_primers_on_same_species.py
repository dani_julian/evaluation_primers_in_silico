#! /bin/python3
#Filtration of pairs of primers from the same species.
#@dani_julian

import sys

#The idea is create a dictionary with key as RefSeq mark (_NC_). If there is no RefSeq sequence, it is evaluated the most common start-end amplicon and is seleted
data = open(sys.argv[1], "rt")
dynamic_dicc = {}
real_name = {} #in order to get the same header in the output as in the header

for line in data:
    if "_NC_" in line:
        line = line.rstrip().split("\t")
        key = line[0].split("_")[0]
        if key not in dynamic_dicc:
            dynamic_dicc[key] = "\t".join(line)

data.seek(0)

#2. Include amplicons not created from RefSeq sequences (Se coge el primer amplicon de todos, CHEQUEAR ESTO) QUIZA DEBERIA VER Y COMPARAR EL START Y END DEL AMPLICON QUE LO TENGO Y MANTENER SI ESTOS DOS SON DISTINTOS
for line in data:
    if not "_NC_" in line:
        line = line.rstrip().split("\t")
        key = line[0].split("_")[0]
        if key not in dynamic_dicc:
            dynamic_dicc[key] = "\t".join(line)

for ele in dynamic_dicc:
    print(dynamic_dicc[ele]) 
data.close()