#! /bin/python3
# @dani_julian

#Script to join by pairs, hits of blast output that has the same reference sequence

import sys
import itertools

dicc = {} #we store for each reference sequence, the primer query that has aligned
data = open(sys.argv[1], "rt")

#1. First part: fill the dicc
for line in data:
    line = line.rstrip().split("\t")
    if line[1] not in dicc:
        dicc[line[1]] = ["\t".join(line)]
    else:
        dicc[line[1]].append("\t".join(line))

data.close()

#2. Second part: print pairs of primers with the same reference
for seq in dicc:
    #print(dicc[seq])
    tmp = list(itertools.combinations(dicc[seq], 2))
    for line in tmp:
        print(str(line).replace("(", "").replace(")", "").replace("'", "").replace(",","\t").replace(r"\t","\t").replace(" ", ""))
