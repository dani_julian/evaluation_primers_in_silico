#! /bin/python3
#Filtration of amplicons from the same species and keep the shortest one
#@dani_julian

import sys

#The idea is filter remove repetitions type: AvsB / BvsA
data = open(sys.argv[1], "rt")
count = 0
already_added = {}

for line in data:
    line = line.rstrip().split("\t")
    forw = line[0] + "_" + line[1] + "_".join(line[2:4]) #it is included the identity and alignment length in order to keep alignments in different of amplicons
    rev = line[1] + "_" + line[0] + "_".join(line[2:4])
    if forw not in already_added:
        already_added[forw] = 0
        already_added[rev] = 0
        print("\t".join(line))

data.close()
