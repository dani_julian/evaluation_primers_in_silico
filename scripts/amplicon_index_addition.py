#! /bin/python3
#Add index number per amplicon of the same pair of primers aligned in the same species
#@dani_julian

import sys

data = open(sys.argv[1], "rt")
dicc = {}

for line in data:
    line = line.rstrip().split("\t")
    hit = line[0] + "_" + line[3]
    if hit not in dicc:
        dicc[hit] = 1
    else:
        dicc[hit] += 1

    print("\t".join(line), dicc[hit], sep = "\t")

data.close()
